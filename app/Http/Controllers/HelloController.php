<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function hello() {
        return "Bonjour à laravel";
    }
    
    public function  hellobynom(Request $request){
        return 'bonjour '.$request["nom"];
    }
}

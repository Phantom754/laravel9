<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    public function lists()
    {
        $listProd= Produit::all();
        return view('produit/index',["produits" =>$listProd]);
    }
}

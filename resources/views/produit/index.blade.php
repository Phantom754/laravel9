<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <h1>List produits</h1>
        <div>
            <button class="btn btn-primary mb-4 mt-2">Ajouter</button>
        </div>
        <table class="table">
            <thead class="table-dark">
                 <tr>
                     <th>ID</th>
                     <th>Libele</th>
                     <th>Prix</th>
                     <th>Quatité</th>
                     <th>Action</th>
                 </tr>
            </thead>
            <tbody>
                    @foreach($produits as $produit)
                        <tr>
                            <th scope="row">{{$produit->id}}</th>
                            <td > {{$produit->libele}} </td>
                            <td > {{$produit->price}} </td>
                            <td > {{$produit->qty}} </td>
                            <td>
                                <button class="btn btn-info">Modifier</button>
                                <button class="btn btn-danger">Supprimer</button>
                            </td>
                        </tr>
                    @endforeach
            </tbody>
        </table>

    </div>
</body>
</html>
